import { Component, OnInit } from '@angular/core';
import { IdcServiceService } from '../idc-service.service';
import { LangLoader } from '../language/lang-loader';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css','../app.component.css']
})
export class ContactusComponent implements OnInit {

  name: string;
  email: string;
  message: string

  constructor(private idcService: IdcServiceService) { }

  ngOnInit(): void {
  }

  get lang(): any {
    return LangLoader.Lang;
  }

  enteredName() {
    if (this.email != "" && this.message != "") this.submitMessage();
    else {
      if (this.email != "") {
        var eleEmail = document.getElementById("email");
        eleEmail.focus();
      }
      else 
      {
        var eleMsg = document.getElementById("message");
        eleMsg.focus();
      }
    }      
}

enteredEmail() {
  if (this.name != "" && this.message != "") this.submitMessage();
  else {
    if (this.name != "") {
        var eleName = document.getElementById("name");
        eleName.focus();
      }
      else 
      {
        var eleMsg = document.getElementById("message");
        eleMsg.focus();
      }
  }      
}

submitMessage()
  {
    alert('Sent msg triggered')

    this.name = "Ivan";
    this.email = "Ivan@test.com";
    this.message = "Msg from Ivan";

    this.idcService.sendMessage(this.name, this.email, this.message).subscribe(
      data => {
            alert('Message submitted');
      });
  }

}
