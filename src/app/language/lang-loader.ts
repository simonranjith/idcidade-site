import {pt} from "./pt"
import {en} from "./en"

export class LangLoader {
    public static _lang: any=null;

    private static getLanguage() {
        var userLang = navigator.language;
        var langg = userLang.substring(0, 2);
        return langg;
    }

    public static get Lang() {
        if (LangLoader._lang == null) {

            var langg = LangLoader.getLanguage();

            if (langg === "pt")
                LangLoader._lang = pt;
            else
                LangLoader._lang = en;
        }

        return LangLoader._lang;
    }

    /*public static void setLang(value) 
    { 
        LangLoader._lang = value; 
    }*/
}