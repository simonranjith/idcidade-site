export var pt = {
    "Login":"Conecte-se",
    "Search":"Pesquisa",
    "Username" : "Nome do usuário",
    "Password" : "Senha",
    "Msg" : "Mensagem",
    "Home" : "Pagina Inicial",
    "Videos" : "Videos",
    "Donate" : "Doações",
    "DonateDesc" : "Se você gostaria de apoiar nossa igreja e nos ajudar a espalhar"+
    " a mensagem do amor e da graça incondicional de Deus ao redor do mundo, clique "+
    " no botão 'Dar' para doar com segurança. Obrigado.",
    "Give" : "Dê para a IDC",
    "ContactUs" : "Contate-Nos",
    "Name" : "Nome",
    "Email" : "Email",
    "Location" : "Localização",
    "Maincontacts" : "Contatos principais",
    "Othercontacts" :"Outros contatos",
    "SendMsg" : "Enviar Mensagem ",
    "Greeting": "Bem-vindo a Casa!",
    "Intro" : "Somos uma comunidade multicultural de pessoas apaixonadas por Jesus" +
    "e pela vida onde todos são importantes e onde juntos aprendemos a cuidar uns dos outros," + 
    "uma igreja onde todos são bem-vindos e onde cada um pode crescer no seu propósito em Deus." +
    "A Igreja da Cidade é uma PCR fundada em Portugal em 1/12/2012 e oficialmente membro da Aliança" + 
    "Evangélica Portuguesa e de outras organizações eclesiásticas, nacionais e internacionais." +
    " (PCR – Pessoa Coletiva Religiosa)." + "Faz-nos uma visita; todos são muito bem-vindos!" +
    " “Porque a tua vida é um propósito de Deus.”",
    "OnlineInfo" : "Todas as nossas reuniões/cultos de domingo e sexta,"+
    "para além de presencialmente, podem ser acompanhadas" +
    "via live-streaming em direto através do nosso canal do YOUTUBE." +
    "Os links das transmissões são sempre colocados antecipadamente " +
    "na página do FACEBOOK (@idcidade). Segue-nos assim no YOUTUBE (@idcidade)." +
    "Segue também o nosso Podcast no SPOTIFY (Igreja da Cidade – City Church Portugal).",
    "WeeklyMeetingHours" : "Horários das Reuniões Semanais",
    "SundayService" : "Domingos, das 10:30 às 12:30 - Celebração",
    "BibleSchool" : "Sextas, das 21:00 às 22:30 - Escola da Bíblia",
    "Fellowship" : "Sextas, às 22:30 - Convívio com lanche oferecido",
    "PrayerVigil": "1ª Sexta de cada mês, às 23:00 - Vigília de Oração",
    "MorningPrayer" : "Quartas, das 8:00 às 9:00 da manhã - Oração Matinal",
    "Copyrights" : " "
}