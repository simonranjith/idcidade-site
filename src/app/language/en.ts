export var en = {
    "Login":"Login",
    "Search":"Search",
    "Username" : "Username",
    "Password" : "Password",
    "Msg" : "Message",
    "Home" : "Home",
    "Videos" : "Videos",
    "Donate" : "Donations",
    "DonateDesc" : "If you’d like to support our church and help us spread the message of "+
    "God’s unconditional love and grace around the world, click on the 'Give' button to give"+
    " safely and securely. Thank you.",
    "Give" : "Give to IDC",
    "ContactUs" : "Contact Us",
    "Name" : "Name",
    "Email" : "Email",
    "Location" : "Location",
    "Maincontacts" : "Main contacts",
    "Othercontacts" :"Other contacts",
    "SendMsg" : "Send Message",
    "Greeting": "Welcome!",
    "Intro": "We are a multicultural community of people passionate about Jesus" + 
    "and life where everyone is important and where together we learn to take care" + 
    "of each other, a church where everyone is welcome and where each one can grow in" + 
    "their purpose in God. " + "The Igreja da Cidade is a PCR founded in Portugal on" +
    "12/1/2012 and officially a member of the Portuguese Evangelical Alliance and other" + 
    "ecclesiastical organizations, national and international. (PCR - Religious Collective Person)",
    "OnlineInfo" : "All our Sunday / Friday meetings / services, " +
    "in addition to in person, they can be accompanied " +
    "via live live-streaming through our YOUTUBE channel." + 
    "Broadcast links are always placed in advance " +
    "on the FACEBOOK page (@idcidade). Follow us on YOUTUBE (@idcidade)."+
    "Also follow our podcast on SPOTIFY (Igreja da Cidade - City Church Portugal).",
    "WeeklyMeetingHours" :  "Weekly meeting hours",
    "SundayService" : "Sundays, from 10:30 to 12:30 - Celebration",
    "BibleSchool" : "Fridays, from 21:00 to 22:30 - Bible School",
    "Fellowship" : "Fridays at 22:30 - Socializing with a snack offered",
    "PrayerVigil": "1st Friday of every month, at 23:00 - Prayer Vigil",
    "MorningPrayer" : "Wednesdays, from 8:00 am to 9:00 am - Morning Prayer",
    "Copyrights" : " "
}