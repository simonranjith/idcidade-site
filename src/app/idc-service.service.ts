import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Headers, Http,URLSearchParams } from '@angular/http'
import {map} from 'rxjs/operators';

@Injectable(/*{
  providedIn: 'root'
}*/)
export class IdcServiceService {

  //private headers: HttpHeaders;
  private headers: Headers;
  private api: string = 'https://localhost:5001/api/'
  //private notesUrl: string = this.api + 'notes';
  private notesUrl: string = this.api + 'videos/getvideos';
  private sendMsgUrl: string = this.api + "common/sendmsg";
  private liveUrl: string = this.api + 'videos/getlive';

  constructor(private http: Http) { 
    //this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
  }
  public get() {
    //return (<any>this.http.get(this.notesUrl, { headers: this.headers })
    
    return (<any>this.http.get(this.notesUrl, {
        headers: this.headers
    }))
    .pipe(map((res: Response) => {
      return res.json();
    }));
  }

  // Get live video
  public getLive() {
    return (<any>this.http.get(this.liveUrl, {
        headers: this.headers
    }))
    .pipe(map((res: Response) => {
      //alert(res.json())
      return res.json();
      //return JSON.parse(res);
    }));
  }

  public sendMessage(name: string, email: string, message:string)
  {
    var params = new URLSearchParams();
          params.set('name', name);
          params.set('email', email);
          params.set('msg', message);

        return (<any>this.http.post(this.sendMsgUrl, params.toString(), {
            headers: this.headers
        }))
        .pipe(map((res: Response) => {
          return res
        }));
            

           /*return (this.http.post(this.sendMsgUrl, params.toString(), {
            headers: this.headers
        }))
            .map((res: Response) => {
              return res
            }
            //res => res.json()
            );*/
  }
}
