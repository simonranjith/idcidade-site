import { Component, OnInit } from '@angular/core';
import { IdcServiceService } from '../idc-service.service';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.css','../app.component.css']
})
export class LiveComponent implements OnInit {

  public live: any;
  constructor(private idcService: IdcServiceService) {
    idcService.getLive().subscribe((data: any) => {
      this.eleName.innerHTML = data[0].content;
      //alert(this.live[0].title)
    });
   }

  ngAfterViewInit(){
  }

  public loadVideo = function() {
    //eleName.innerHTML = this.live;
  };


  eleName : any; 
  ngOnInit(): void {
    this.eleName = document.getElementById("live-div");
  }

}
