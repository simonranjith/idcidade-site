import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import {IvyCarouselModule} from 'angular-responsive-carousel';
import { LiveComponent } from './live/live.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FooterComponent } from './footer/footer.component';
import { MenubarComponent } from './menubar/menubar.component';
import { ContactusComponent } from './contactus/contactus.component';
import { DonateComponent } from './donate/donate.component';
import { VideoComponent } from './video/video.component';
import { VideoListComponent } from './video-list/video-list.component';
import {TableModule} from 'primeng/table';
import {IdcServiceService} from './idc-service.service';
import { DialogComponent } from './dialog.component'

const appRoutes: Routes = [
  //{ path: '', component: AppComponent },
  {path: "", redirectTo: "home", pathMatch: "full"},
  { path: 'home', component: HomeComponent },
  { path: 'live', component: LiveComponent },
  { path: 'donate', component: DonateComponent },
  { path: 'contactus', component: ContactusComponent },
  { path: 'videos', component: VideoComponent },
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LiveComponent,
    PageNotFoundComponent,
    HomeComponent,
    ToolbarComponent,
    FooterComponent,
    MenubarComponent,
    ContactusComponent,
    DonateComponent,
    VideoComponent,
    VideoListComponent,
    DialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes, { relativeLinkResolution: 'legacy' }),
    AppRoutingModule,
    IvyCarouselModule,
    HttpClientModule,
    HttpModule,
    TableModule,
    CommonModule,
    FormsModule
  ],
  providers: [IdcServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
