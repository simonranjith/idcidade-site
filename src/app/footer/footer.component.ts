import { Component, OnInit } from '@angular/core';
import { LangLoader } from '../language/lang-loader';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get lang(): any {
    return LangLoader.Lang;
  }
}
