import { Component, OnInit } from '@angular/core';
import { LangLoader } from '../language/lang-loader';
import {pt} from "../language/pt"
import {en} from "../language/en"

@Component({
  selector: 'app-menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.css']
})
export class MenubarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get lang(): any {
    return LangLoader.Lang;
  }

  langen() {
    LangLoader._lang = en;
    console.log(LangLoader.Lang);
  }
  langpt() {
    LangLoader._lang = pt;
    console.log(LangLoader.Lang);
  }
}
