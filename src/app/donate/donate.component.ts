import { Component, OnInit } from '@angular/core';
import { LangLoader } from '../language/lang-loader';

@Component({
  selector: 'app-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.css','../app.component.css']
})
export class DonateComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get lang(): any {
    return LangLoader.Lang;
  }

  give()
  {}
}
