import { TestBed } from '@angular/core/testing';

import { IdcServiceService } from './idc-service.service';

describe('IdcServiceService', () => {
  let service: IdcServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IdcServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
