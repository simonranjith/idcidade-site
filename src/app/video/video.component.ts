import { Component, OnInit } from '@angular/core';
import { IdcServiceService } from '../idc-service.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css','../app.component.css']
})
export class VideoComponent implements OnInit {

  public videos: Array<any>;
  statusVisible : boolean;
  public currentVideo: any;
  currentVideoTitle : string;

  constructor(private idcService: IdcServiceService) { 
    this.currentVideoTitle = "";
    idcService.get().subscribe((data: any) => this.videos = data);
  }

  public loadVideo = function(video: any) {
    this.statusVisible = true;
    this.currentVideo = video;
    var eleName = document.getElementById("curr-video-div");
    eleName.innerHTML = video.content;
    this.currentVideoTitle = video.title + ", " + video.date;
  };

  OnEditCancel() {
    this.statusVisible = false;
  }

  ngOnInit(): void {
  }

}
