import { Component, OnInit } from '@angular/core';
import { LangLoader } from '../language/lang-loader';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css', '../app.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get lang(): any {
    return LangLoader.Lang;
  }
}
