import { Component, OnInit,Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {

  @Input() videos: Array<any>;
  @Output() editClicked = new EventEmitter<any>();
  //constructor() { }

  ngOnInit(): void {
  }

  public edit(data) {
    this.editClicked.emit(Object.assign({}, data));
  }
}
