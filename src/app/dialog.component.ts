import { Component, EventEmitter } from '@angular/core';


@Component({
    selector: 'dlg',
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.css'],
    inputs: ['visible', 'caption', 'title', 'dialogType','closable','modal'],
    outputs: ['onClosed', 'onSubmit']
})

export class DialogComponent {

    onClosed = new EventEmitter();
    onSubmit = new EventEmitter();

    visible: boolean;
    confirmButtonCaption: string;
    title: string;
    dialogType: number;
    closable : boolean;
    modal : boolean;

    status: string;

    get caption(): string {
        return this.confirmButtonCaption;
    }

    set caption(text: string) {
        this.confirmButtonCaption = text;
    }

    constructor() {
        this.visible = false;
        this.caption = 'OK';
        this.title = 'Igreja de Cidade';
        this.dialogType = 1;
        this.status = '';
        this.closable = true;
        this.modal = false;
    }

    onClosedButtonClicked() {
        this.onClosed.emit('event');
    }

    submit()
    {
        this.status = "Processing...";

        if (this.dialogType == 2)
            this.onSubmit.emit('event');
            
        this.status = "";
    }
  
}